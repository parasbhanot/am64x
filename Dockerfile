# Docker v1
FROM ubuntu:22.04

RUN apt update

# Generic ubuntu 
RUN apt install -y ca-certificates \
    iputils-ping \
    net-tools \
    htop \
    vim 

# Buildroot dependencies 
RUN apt install -y wget \
	curl \
	git \
	build-essential \
	unzip \
	rsync \
	bc \
	python3 \
	libncurses-dev \
	file \
	cpio \
	u-boot-tools



WORKDIR /buildroot_mine

# Add also auto-extract the compressed 

ADD buildroot-2022.02.6.tar.gz .
ADD xenomai-v3.2.1.tar.bz2 .
COPY am64x_droot ./am64x_droot

#RUN tar -xvf buildroot-2022.02.6.tar.gz

WORKDIR /buildroot_mine/buildroot-2022.02.6

#Run BR2

RUN BR2_EXTERNAL=../am64x_droot make O=../am64x_droot/output rootfs_am64x_defconfig

WORKDIR ../am64x_droot/output
